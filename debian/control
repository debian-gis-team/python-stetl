Source: python-stetl
Maintainer: Debian GIS Project <pkg-grass-devel@lists.alioth.debian.org>
Uploaders: Bas Couwenberg <sebastic@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               dh-sequence-python3,
               dh-sequence-sphinxdoc,
               pylint,
               python3-all,
               python3-deprecated,
               python3-flake8,
               python3-gdal,
               python3-jinja2,
               python3-lxml,
               python3-mock,
               python3-nose2,
               python3-psycopg2,
               python3-setuptools,
               python3-sphinx,
               docbook2x,
               docbook-xsl,
               docbook-xml,
               xsltproc
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-gis-team/python-stetl
Vcs-Git: https://salsa.debian.org/debian-gis-team/python-stetl.git
Homepage: http://stetl.org/
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-pybuild

Package: python3-stetl
Architecture: all
Section: python
Depends: ${python3:Depends},
         ${sphinxdoc:Depends},
         ${misc:Depends}
Description: Streaming ETL - Geospatial ETL framework for Python 3
 Stetl, streaming ETL, pronounced "staedl", is a lightweight ETL-framework
 for the conversion of rich (as GML) geospatial data conversion.
 .
 It basically glues together existing parsing and transformation tools
 like GDAL/OGR (ogr2ogr) and XSLT. By using native tools like libxml and
 libxslt (via Python lxml) Stetl is speed-optimized.
 .
 Stetl has a similar design as Spring (Java) and other modern frameworks
 based on IoC (Inversion of Control). A configuration file (in Python
 config format) specifies your chain of ETL steps. This chain is formed
 by a series of Python modules/objects and their parameters. These are
 symbolically specified in the config file. You just invoke etl.py the
 main program with a config file. The config file specifies the input
 modules (e.g. PostGIS), transformers (e.g. XSLT) and outputs (e.g. a GML
 file or even WFS-T a geospatial protocol to publish GML to a server).
 .
 This package contains the module for Python 3.

Package: stetl
Architecture: all
Section: utils
Depends: python3-stetl (>= ${binary:Version}),
         ${python3:Depends},
         ${misc:Depends}
Description: Streaming ETL - Commandline utility
 Stetl, streaming ETL, pronounced "staedl", is a lightweight ETL-framework
 for the conversion of rich (as GML) geospatial data conversion.
 .
 It basically glues together existing parsing and transformation tools
 like GDAL/OGR (ogr2ogr) and XSLT. By using native tools like libxml and
 libxslt (via Python lxml) Stetl is speed-optimized.
 .
 Stetl has a similar design as Spring (Java) and other modern frameworks
 based on IoC (Inversion of Control). A configuration file (in Python
 config format) specifies your chain of ETL steps. This chain is formed
 by a series of Python modules/objects and their parameters. These are
 symbolically specified in the config file. You just invoke etl.py the
 main program with a config file. The config file specifies the input
 modules (e.g. PostGIS), transformers (e.g. XSLT) and outputs (e.g. a GML
 file or even WFS-T a geospatial protocol to publish GML to a server).
 .
 This package contains the stetl utility.
